extends Node2D

var rng = RandomNumberGenerator.new()
var ordre_cartes
var carte
var nbrPages=1
#var reso = Vector2(1125,1015)
#var reso = Vector2(1125/0.15,1015/0.15)
#var reso = Vector2(2000,2000)
var scale_zoom = 0.6
var titre

#my linux
#var cartesPath="/home/ewen/godot/loto-cartes-export-linux/cartes/"
#var pagesPath ="/home/ewen/godot/loto-cartes-export-linux/pages/"

#mac test
#var cartesPath="Users/ewen/Downloads/loto/cartes"
#var pagesPath="Users/ewen/Downloads/loto/pages/"

#editeur
var cartesPath=ProjectSettings.globalize_path("res://cartes/")
var pagesPath=ProjectSettings.globalize_path("res://pages/")


#francois (voir avec lui)

#var cartesPath=OS.get_executable_path().get_base_dir()+"../cartes/"

#var dir = DirAccess.open("/home/ewen/godot/loto-cartes"+"/cartes")
var dir = DirAccess.open(cartesPath)

# Called when the node enters the scene tree for the first time.
func _ready():
#	$GridContainer.scale=Vector2(scale_zoom,scale_zoom)
	print("testtestetestettstststst")
	melange()
#	$ButtonCapturer.show()
#	$ButtonMelanger.show()
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


#func test_si_non_import(carte):
#	return carte.contains(".import")
	

func nouvel_ordre():
	print(dir)
	var ordre_cartes_dans_ordre_with_import = Array(dir.get_files())
	var ordre_cartes_dans_ordre =[]
	for i in range(len(ordre_cartes_dans_ordre_with_import)):
		if(!ordre_cartes_dans_ordre_with_import[i].contains(".import")):
			ordre_cartes_dans_ordre.append(ordre_cartes_dans_ordre_with_import[i])
			
#	var ordre_cartes_dans_ordre = ordre_cartes_dans_ordre_with_import.filter(test_si_non_import())
		
	var ordre_cartes = []
	for i in range(len(ordre_cartes_dans_ordre)):
		while true:
			print(ordre_cartes_dans_ordre)
			print(ordre_cartes)
			var a = randi_range(0,len(ordre_cartes_dans_ordre)-1)
			if ordre_cartes_dans_ordre[a] not in ordre_cartes:
				ordre_cartes.append(ordre_cartes_dans_ordre[a])
				break
	print(OS.get_executable_path().get_base_dir())
	print(ordre_cartes)
	return ordre_cartes
	
	
#	ordre_cartes=Array()
#	var i = 0
#	while i<54:
#		carte=rng.randi_range(1,54)
#		if carte not in ordre_cartes:
#			ordre_cartes.append(carte)
#			i+=1
#	return ordre_cartes

func change_texture(ordre):
	for i in range(1,17):
		$SubViewportContainerCartes/SubViewportCartes/GridContainer1.get_node(NodePath(str(i))).texture=ImageTexture.create_from_image(Image.load_from_file(cartesPath+str(ordre[i-1])))
	for i in range(17,33):
		$SubViewportContainerCartes/SubViewportCartes/GridContainer2.get_node(NodePath(str(i))).texture=ImageTexture.create_from_image(Image.load_from_file(cartesPath+str(ordre[i-1])))


var tim = Timer.new()
func melange():
	var ordre = nouvel_ordre()
	print(ordre)
	change_texture(ordre)
	tim.stop()


func _on_button_capturer_pressed():
	
#	get_viewport().size = reso / scale_zoom
#	$GridContainer.scale/=scale_zoom

#	$ButtonCapturer.hide()
#	$ButtonMelanger.hide()
#	get_viewport().size.x*=scale_zoom
	
#	await get_tree().create_timer(1.0).timeout
	
	
#	var imgTmp = Image.new()
#	imgTmp.create(img.get_size().x, img.get_size().y, false, img.get_format())
#	imgTmp.blit_rect(img, Rect2(0.0, 0.0, 10, img.get_size().y), Vector2i(10,img.get_size().y))
#	var tex = ImageTexture.create_from_image(img)
#	img.crop(get_viewport().size.x,get_viewport().size.x)
	
	
	
	
	if !DirAccess.dir_exists_absolute(pagesPath):
		DirAccess.make_dir_absolute(pagesPath)
	
	var num=3
	
	for i in range(nbrPages):
		$SubViewportContainerCartes/SubViewportCartes/Titre1.text=titre+" "+str(num)
		$SubViewportContainerCartes/SubViewportCartes/Titre2.text=titre+" "+str(num+1)
		num+=2
		if $SubViewportContainerCartes.find_child("tim")==null:
			$SubViewportContainerCartes.add_child(tim)
		var img = $SubViewportContainerCartes/SubViewportCartes.get_viewport().get_texture().get_image()
		melange()
		tim.start()
		await tim.timeout
		img.save_png(pagesPath+str(i)+".png")
#
#	var f = FileAccess.open("user://loto.png", FileAccess.READ)
#	var buf = f.get_buffer(f.get_length())
#	JavaScriptBridge.download_buffer(buf, "loto.png", "image/png")
#	print("hahahaha")
	
#	await get_tree().create_timer(1.0).timeout
#
#	get_viewport().size.y/=scale_zoom
##	get_viewport().size = get_viewport().size * scale_zoom
##	$GridContainer.scale*=scale_zoom
#	$ButtonCapturer.show()
#	$ButtonMelanger.show()

	# Set the texture to the captured image node.
#	$CapturedImage.set_texture(tex)
#
#	print(img)


func _on_text_edit_nbr_pages_text_changed():
	nbrPages=int($SubViewportContainerBoutons/SubViewportBoutons/TextEditNbrPages.text)


func _on_text_edit_titre_text_changed():
	titre=$SubViewportContainerBoutons/SubViewportBoutons/TextEditTitre.text
	$SubViewportContainerCartes/SubViewportCartes/Titre1.text=titre+" 1"
	$SubViewportContainerCartes/SubViewportCartes/Titre2.text=titre+" 2"
